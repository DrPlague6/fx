# fx
Draw any math function
![img1](src1.png/)
![img1](src2.png/)

## Known Bugs
Functions that cause issues
- tg(x)
- ctg(x)
- 1/x

## Credit

Project works thanks to [mXparser by Mariusz Gromada](https://github.com/mariuszgromada/MathParser.org-mXparser)
