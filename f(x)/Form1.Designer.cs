﻿namespace f_x_
{
    partial class fx
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fx));
            this.label1 = new System.Windows.Forms.Label();
            this.formulaTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.aBar = new System.Windows.Forms.TrackBar();
            this.aLable = new System.Windows.Forms.Label();
            this.bLable = new System.Windows.Forms.Label();
            this.bBar = new System.Windows.Forms.TrackBar();
            this.cLable = new System.Windows.Forms.Label();
            this.cBar = new System.Windows.Forms.TrackBar();
            this.qLable = new System.Windows.Forms.Label();
            this.qBar = new System.Windows.Forms.TrackBar();
            this.cartesianBox = new System.Windows.Forms.PictureBox();
            this.aPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.bPanel = new System.Windows.Forms.Panel();
            this.cPanel = new System.Windows.Forms.Panel();
            this.qPanel = new System.Windows.Forms.Panel();
            this.mouseValue = new System.Windows.Forms.Label();
            this.help = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartesianBox)).BeginInit();
            this.aPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.bPanel.SuspendLayout();
            this.cPanel.SuspendLayout();
            this.qPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "f(x)=";
            // 
            // formulaTextBox
            // 
            this.formulaTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.formulaTextBox.Location = new System.Drawing.Point(57, 9);
            this.formulaTextBox.Name = "formulaTextBox";
            this.formulaTextBox.Size = new System.Drawing.Size(657, 29);
            this.formulaTextBox.TabIndex = 1;
            this.formulaTextBox.TextChanged += new System.EventHandler(this.formulaTextBox_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkCyan;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(717, 9);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(258, 30);
            this.button1.TabIndex = 2;
            this.button1.Text = "CALCULATE";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // aBar
            // 
            this.aBar.BackColor = System.Drawing.Color.White;
            this.aBar.Location = new System.Drawing.Point(3, 28);
            this.aBar.Maximum = 25;
            this.aBar.Minimum = -25;
            this.aBar.Name = "aBar";
            this.aBar.Size = new System.Drawing.Size(201, 45);
            this.aBar.TabIndex = 4;
            this.aBar.ValueChanged += new System.EventHandler(this.aBar_ValueChanged);
            // 
            // aLable
            // 
            this.aLable.AutoSize = true;
            this.aLable.Location = new System.Drawing.Point(3, 0);
            this.aLable.Name = "aLable";
            this.aLable.Size = new System.Drawing.Size(13, 15);
            this.aLable.TabIndex = 5;
            this.aLable.Text = "a";
            // 
            // bLable
            // 
            this.bLable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bLable.AutoSize = true;
            this.bLable.Location = new System.Drawing.Point(3, 0);
            this.bLable.Name = "bLable";
            this.bLable.Size = new System.Drawing.Size(14, 15);
            this.bLable.TabIndex = 7;
            this.bLable.Text = "b";
            // 
            // bBar
            // 
            this.bBar.BackColor = System.Drawing.Color.White;
            this.bBar.Location = new System.Drawing.Point(3, 104);
            this.bBar.Maximum = 25;
            this.bBar.Minimum = -25;
            this.bBar.Name = "bBar";
            this.bBar.Size = new System.Drawing.Size(201, 45);
            this.bBar.TabIndex = 6;
            this.bBar.ValueChanged += new System.EventHandler(this.bBar_ValueChanged);
            // 
            // cLable
            // 
            this.cLable.AutoSize = true;
            this.cLable.Location = new System.Drawing.Point(4, 0);
            this.cLable.Name = "cLable";
            this.cLable.Size = new System.Drawing.Size(13, 15);
            this.cLable.TabIndex = 9;
            this.cLable.Text = "c";
            // 
            // cBar
            // 
            this.cBar.BackColor = System.Drawing.Color.White;
            this.cBar.Location = new System.Drawing.Point(3, 180);
            this.cBar.Maximum = 25;
            this.cBar.Minimum = -25;
            this.cBar.Name = "cBar";
            this.cBar.Size = new System.Drawing.Size(201, 45);
            this.cBar.TabIndex = 8;
            this.cBar.ValueChanged += new System.EventHandler(this.cBar_ValueChanged);
            // 
            // qLable
            // 
            this.qLable.AutoSize = true;
            this.qLable.Location = new System.Drawing.Point(4, 0);
            this.qLable.Name = "qLable";
            this.qLable.Size = new System.Drawing.Size(14, 15);
            this.qLable.TabIndex = 11;
            this.qLable.Text = "q";
            // 
            // qBar
            // 
            this.qBar.BackColor = System.Drawing.Color.White;
            this.qBar.Location = new System.Drawing.Point(3, 256);
            this.qBar.Maximum = 25;
            this.qBar.Minimum = -25;
            this.qBar.Name = "qBar";
            this.qBar.Size = new System.Drawing.Size(201, 45);
            this.qBar.TabIndex = 10;
            this.qBar.ValueChanged += new System.EventHandler(this.qBar_ValueChanged);
            // 
            // cartesianBox
            // 
            this.cartesianBox.BackColor = System.Drawing.Color.White;
            this.cartesianBox.Location = new System.Drawing.Point(3, 44);
            this.cartesianBox.Name = "cartesianBox";
            this.cartesianBox.Size = new System.Drawing.Size(972, 774);
            this.cartesianBox.TabIndex = 12;
            this.cartesianBox.TabStop = false;
            this.cartesianBox.Paint += new System.Windows.Forms.PaintEventHandler(this.cartesianBox_Paint);
            this.cartesianBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cartesianBox_MouseDown);
            // 
            // aPanel
            // 
            this.aPanel.AutoScroll = true;
            this.aPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.aPanel.Controls.Add(this.aLable);
            this.aPanel.Location = new System.Drawing.Point(3, 3);
            this.aPanel.Name = "aPanel";
            this.aPanel.Size = new System.Drawing.Size(201, 19);
            this.aPanel.TabIndex = 13;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.aPanel);
            this.flowLayoutPanel1.Controls.Add(this.aBar);
            this.flowLayoutPanel1.Controls.Add(this.bPanel);
            this.flowLayoutPanel1.Controls.Add(this.bBar);
            this.flowLayoutPanel1.Controls.Add(this.cPanel);
            this.flowLayoutPanel1.Controls.Add(this.cBar);
            this.flowLayoutPanel1.Controls.Add(this.qPanel);
            this.flowLayoutPanel1.Controls.Add(this.qBar);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(769, 44);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(206, 297);
            this.flowLayoutPanel1.TabIndex = 14;
            // 
            // bPanel
            // 
            this.bPanel.Controls.Add(this.bLable);
            this.bPanel.Location = new System.Drawing.Point(3, 79);
            this.bPanel.Name = "bPanel";
            this.bPanel.Size = new System.Drawing.Size(201, 19);
            this.bPanel.TabIndex = 15;
            // 
            // cPanel
            // 
            this.cPanel.Controls.Add(this.cLable);
            this.cPanel.Location = new System.Drawing.Point(3, 155);
            this.cPanel.Name = "cPanel";
            this.cPanel.Size = new System.Drawing.Size(201, 19);
            this.cPanel.TabIndex = 15;
            // 
            // qPanel
            // 
            this.qPanel.Controls.Add(this.qLable);
            this.qPanel.Location = new System.Drawing.Point(3, 231);
            this.qPanel.Name = "qPanel";
            this.qPanel.Size = new System.Drawing.Size(201, 19);
            this.qPanel.TabIndex = 15;
            // 
            // mouseValue
            // 
            this.mouseValue.AutoSize = true;
            this.mouseValue.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.mouseValue.Location = new System.Drawing.Point(6, 47);
            this.mouseValue.Name = "mouseValue";
            this.mouseValue.Size = new System.Drawing.Size(56, 19);
            this.mouseValue.TabIndex = 15;
            this.mouseValue.Text = "☝ (x,y)";
            // 
            // help
            // 
            this.help.AutoSize = true;
            this.help.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.help.Location = new System.Drawing.Point(696, 12);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(18, 22);
            this.help.TabIndex = 16;
            this.help.Text = "?";
            this.help.Click += new System.EventHandler(this.help_Click);
            // 
            // fx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(980, 823);
            this.Controls.Add(this.help);
            this.Controls.Add(this.mouseValue);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.formulaTextBox);
            this.Controls.Add(this.cartesianBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fx";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.aBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartesianBox)).EndInit();
            this.aPanel.ResumeLayout(false);
            this.aPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.bPanel.ResumeLayout(false);
            this.bPanel.PerformLayout();
            this.cPanel.ResumeLayout(false);
            this.cPanel.PerformLayout();
            this.qPanel.ResumeLayout(false);
            this.qPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox formulaTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TrackBar aBar;
        private System.Windows.Forms.Label aLable;
        private System.Windows.Forms.Label bLable;
        private System.Windows.Forms.TrackBar bBar;
        private System.Windows.Forms.Label cLable;
        private System.Windows.Forms.TrackBar cBar;
        private System.Windows.Forms.Label qLable;
        private System.Windows.Forms.TrackBar qBar;
        private System.Windows.Forms.PictureBox cartesianBox;
        private System.Windows.Forms.Panel aPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel bPanel;
        private System.Windows.Forms.Panel cPanel;
        private System.Windows.Forms.Panel qPanel;
        private System.Windows.Forms.Label mouseValue;
        private System.Windows.Forms.Label help;
    }
}
