﻿using f_x_.model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace f_x_
{
    public partial class fx : Form
    {
        private float stepX;
        private float stepY;
        float centerX;
        float centerY;
        Formula currentFormula = new();

        public fx()
        {
            InitializeComponent();
            aPanel.Visible = false;
            bPanel.Visible = false;
            cPanel.Visible = false;
            qPanel.Visible = false;
            flowLayoutPanel1.Visible = false;

            aBar.Visible = false;
            bBar.Visible = false;
            cBar.Visible = false;
            qBar.Visible = false;
            stepX = cartesianBox.Width / 100;
            stepY = cartesianBox.Height / 100;
            centerX = cartesianBox.Width / 2;
            centerY = cartesianBox.Height / 2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string formText = formulaTextBox.Text.Trim();
            currentFormula = new Formula(formulaTextBox.Text.Trim());

            if (formText.Contains("a"))
                currentFormula.AVar = aBar.Value;
            if (formText.Contains("b"))
                currentFormula.BVar = bBar.Value;
            if (formText.Contains("c"))
                currentFormula.CVar = cBar.Value;
            if (formText.Contains("q"))
                currentFormula.QVar = qBar.Value;
            cartesianBox.Refresh();
        }

        private void aBar_ValueChanged(object sender, EventArgs e)
        {
            if (currentFormula != null)
            {
                currentFormula.AVar = aBar.Value;
                cartesianBox.Refresh();
            }
            aLable.Text = "a: " + aBar.Value;
        }

        private void bBar_ValueChanged(object sender, EventArgs e)
        {
            if (currentFormula != null)
            {
                currentFormula.BVar = bBar.Value;
                cartesianBox.Refresh();
            }
            bLable.Text = "b: " + bBar.Value;
        }

        private void cBar_ValueChanged(object sender, EventArgs e)
        {
            if (currentFormula != null)
            {
                currentFormula.CVar = cBar.Value;
                cartesianBox.Refresh();
            }
            cLable.Text = "c: " + cBar.Value;
        }

        private void qBar_ValueChanged(object sender, EventArgs e)
        {
            if (currentFormula != null)
            {
                currentFormula.QVar = qBar.Value;
                cartesianBox.Refresh();
            }
            qLable.Text = "q: " + qBar.Value;
        }

        private void formulaTextBox_TextChanged(object sender, EventArgs e)
        {
            if (formulaTextBox.Text.Contains('a') || formulaTextBox.Text.Contains('b') || formulaTextBox.Text.Contains('c') || formulaTextBox.Text.Contains('q'))
                flowLayoutPanel1.Visible = true;
            else
                flowLayoutPanel1.Visible = false;

            if (formulaTextBox.Text.Contains('a'))
            {
                aPanel.Visible = true;
                aBar.Visible = true;
            }
            else
            {
                aPanel.Visible = false;
                aBar.Visible = false;
            }
            if (formulaTextBox.Text.Contains('b'))
            {
                bPanel.Visible = true;
                bBar.Visible = true;
            }
            else
            {
                bPanel.Visible = false;
                bBar.Visible = false;
            }
            if (formulaTextBox.Text.Contains('c'))
            {
                cPanel.Visible = true;
                cBar.Visible = true;
            }
            else
            {
                cPanel.Visible = false;
                cBar.Visible = false;
            }
            if (formulaTextBox.Text.Contains('q'))
            {
                qPanel.Visible = true;
                qBar.Visible = true;
            }
            else
            {
                qPanel.Visible = false;
                qBar.Visible = false;
            }
        }

        private void drawCartesian(PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 1.0f);

            //Rysowanie lini poziomej
            PointF x1 = new(0, cartesianBox.Height / 2);
            PointF y1 = new(cartesianBox.Width, centerY);
            e.Graphics.DrawLine(pen, x1, y1);

            //Rysowanie lini pionowej
            x1 = new(centerX, 0);
            y1 = new(centerX, cartesianBox.Height);
            e.Graphics.DrawLine(pen, x1, y1);

            int i = 0;

            //Rysowanie podziałki X
            while (stepX * i < cartesianBox.Width)
            {
                x1 = new(centerX + (stepX * i), centerY - 2);
                y1 = new(centerX + (stepX * i), centerY + 2);
                e.Graphics.DrawLine(pen, x1, y1);
                x1 = new(centerX - (stepX * i), centerY - 2);
                y1 = new(centerX - (stepX * i), centerY + 2);
                e.Graphics.DrawLine(pen, x1, y1);
                i++;
            }

            stepY = cartesianBox.Height / 100;
            i = 0;

            //Rysowanie podziałki Y
            while (stepY * i < cartesianBox.Height)
            {
                x1 = new(centerX - 2, centerY + (stepY * i));
                y1 = new(centerX + 2, centerY + (stepY * i));
                e.Graphics.DrawLine(pen, x1, y1);
                x1 = new(centerX - 2, centerY - (stepY * i));
                y1 = new(centerX + 2, centerY - (stepY * i));
                e.Graphics.DrawLine(pen, x1, y1);
                i++;
            }
        }

        //Rysowanie funkcji
        private void drawFunction(Formula form, PaintEventArgs e)
        {
            if (String.IsNullOrEmpty(form.expression()))
                return;
            try
            {
                Pen pen = new Pen(Color.DarkCyan, 2.0f);

                List<PointF> yAxis = new List<PointF> { };
                List<float> zeroPlaces = new List<float> { };

                //Szukanie pierwszego X środek / krok 
                float i = Convert.ToInt32(-1 * centerX / stepX);
                while (stepX * i < cartesianBox.Width)
                {
                    //Wartosc funkcji dla X
                    float x1 = Convert.ToSingle(form.GetValue(i));

                    //Pozycja punktu (środek+krok*X,środek-f(x)*krok)
                    yAxis.Add(new(centerX + (i * stepX), centerY - (x1 * stepY)));

                    i += 1f;
                }
                //Rysowanie listy punktow
                e.Graphics.DrawCurve(pen, yAxis.ToArray());
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(OverflowException))
                    MessageBox.Show("Error: Too large Numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void cartesianBox_Paint(object sender, PaintEventArgs e)
        {
            drawFunction(currentFormula, e);
            drawCartesian(e);
        }

        private void cartesianBox_MouseDown(object sender, MouseEventArgs e)
        {
            float x = MathF.Round((e.X - centerX) / stepX, 2);
            mouseValue.Text = "☝ ( " + x + ", " + Math.Round(currentFormula.GetValue(x), 2) + " )";
        }

        private void help_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Obsługiwane funkcje:\n ax+b \n ax^2+bx+c \n a(x-b)*(x-c) \n a(x-p)^2+q \n a*sin(x)+b \n cos(x) \n Varibles: a, b, c, q \n Ex: a*x^2+b \n !", "Help", MessageBoxButtons.OK);
        }
    }
}
